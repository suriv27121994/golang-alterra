package main

import "fmt"

func BilGanjilGenap(inputValue int){
	if inputValue%2 == 0 {
		fmt.Println(inputValue, "Bilangan Genap")
	} else {
		fmt.Println(inputValue, "Bilangan Ganjil")
	}
} 

func main() {
	value:= 7
	BilGanjilGenap(value)
}